import React from 'react';

import Form from "./routes/Form";

function App() {
  return (
     <div className="App">
        <Form />
     </div>
  );
}

export default App;
