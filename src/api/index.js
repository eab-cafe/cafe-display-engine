import axios from "axios";
import demoData from "./dummyData/demo_data_v1.json";
import config from "../config";

axios.defaults.baseURL = config.url;

axios.interceptors.request.use(
  config => {
    config.headers["Content-Type"] = "application/json";
    return config;
  },
  error => {
    return Promise.reject(error);
  }
);

export function loadDummyData() {
  return demoData;
}

export function getFormList() {
  return axios
    .get(`/form`)
    .then(result => result.data)
    .catch(() => {
      throw new Error(`Error occur on getting Form List`);
    });
}

export function getFormByID({ id }) {
  return axios
    .get(`/form/${id}`)
    .then(result => result.data)
    .catch(() => {
      throw new Error(`Error occur on getting form. path: ${id}`);
    });
}

export function getPageByID({ id }) {
  return axios
    .get(`/page/${id}`)
    .then(result => result.data)
    .catch(() => {
      throw new Error(`Error occur on getting page. path: ${id}`);
    });
}

export function saveForm({ id, form }) {
  return axios.put(`/form/${id}`, form).then(result => result.data);
}

export function createNewForm({ data }) {
  return axios.post("/form", data).then(result => result.data);
}

export function deleteFormByID({id}) {
  return axios.delete(`/form/${id}`).then(result => result.data);
}
