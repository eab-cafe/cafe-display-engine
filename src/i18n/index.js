import I18N from "i18n";
import en_US from "./store/en_US";
// import zh_HK from "./store/zh_HK";
// import zh_CN from "./store/zh_CN";

/**
 * @description if can not found the text, throw error
 * @param {string} path - the path of the text
 * */
const pathNotFoundError = path => {
  throw new Error(`text not found. path: "${path}"`);
};
/**
 * @description get the text form the textStore
 * @param {string} path - the path of the text
 * @param {object} replace - replace target words in text
 * @example
 * i18n({
 *   language: "user.language",
 *   path: "user.addressButton",
 *   replace: {
 *     name: "John",
 *   },
 * })
 * @return {string|object}
 * */
export default function i18n({ path, replace = {} }) {
  const { language } = I18N;
  const pathArray = path.split(".");
  const defaultStore = en_US;
  let textData;

  if (language === "zh_HK") {
    // textData = zh_HK;
  } else if (language === "zh_CN") {
    // textData = zh_CN;
  } else {
    textData = en_US;
  }

  // get the data
  try {
    pathArray.forEach(pathData => {
      if (textData[pathData] === undefined) {
        throw new Error("textStoreEnd");
      }
      textData = textData[pathData];
    });
  } catch (e) {
    if (e.message !== "textStoreEnd") {
      pathNotFoundError(path);
    }
    textData = defaultStore;
    pathArray.forEach(pathData => {
      if (textData[pathData] === undefined) {
        pathNotFoundError(path);
      }
      textData = textData[pathData];
    });
  }
  // check the return type
  if (typeof textData !== "string" && typeof textData !== "number") {
    pathNotFoundError(path);
  }

  // replace words
  const replaceList = Object.keys(replace);
  if (replaceList.length !== 0) {
    replaceList.forEach(replaceKey => {
      textData = textData.replace(`{${replaceKey}}`, replace[replaceKey]);
    });
  }

  return textData;
}
