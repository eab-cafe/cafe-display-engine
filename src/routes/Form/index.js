import React from 'react';

import FormContent from "./components/FormContent";
import styles from "./Form.module.css";

function Form() {
  return <div className={styles.formContainer}>
     <div className={styles.formContent}>
        <FormContent />
     </div>
  </div>
}

export default Form;
